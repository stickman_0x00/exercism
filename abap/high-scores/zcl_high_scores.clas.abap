CLASS zcl_high_scores DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    TYPES integertab TYPE STANDARD TABLE OF i WITH EMPTY KEY.
    METHODS constructor
      IMPORTING
        scores TYPE integertab.

    METHODS list_scores
      RETURNING
        VALUE(result) TYPE integertab.

    METHODS latest
      RETURNING
        VALUE(result) TYPE i.

    METHODS personalbest
      RETURNING
        VALUE(result) TYPE i.

    METHODS personaltopthree
      RETURNING
        VALUE(result) TYPE integertab.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA scores_list TYPE integertab.

ENDCLASS.


CLASS zcl_high_scores IMPLEMENTATION.

  METHOD constructor.
    me->scores_list = scores.
  ENDMETHOD.

  METHOD latest.
    result = me->scores_list[ lines( me->scores_list ) ].
  ENDMETHOD.

  METHOD list_scores.
    result = me->scores_list.
  ENDMETHOD.

  METHOD personalbest.
    LOOP AT me->scores_list INTO DATA(score).
      IF result < score.
        result = score.
      ENDIF.
    ENDLOOP.
  ENDMETHOD.

  METHOD personaltopthree.
    DATA(LV_COUNT) = 1.
    WHILE lines( me->scores_list ) >= LV_COUNT AND LV_COUNT < 4.
      ADD 1 TO LV_COUNT.
      result = VALUE #( BASE result (  ) ).
    ENDWHILE.
    
    LOOP AT me->scores_list INTO DATA(score).
      LOOP AT result ASSIGNING FIELD-SYMBOL(<R>).
        IF <R> < score.
          DATA(LV_temp) = <R>.
          <R> = score.
          score = lv_temp.
        ENDIF.
      ENDLOOP.
    ENDLOOP.
  ENDMETHOD.


ENDCLASS.
