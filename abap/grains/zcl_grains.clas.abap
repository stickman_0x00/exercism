CLASS zcl_grains DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    TYPES type_result TYPE p LENGTH 16 DECIMALS 0.
    METHODS square
      IMPORTING
        input         TYPE i
      RETURNING
        VALUE(result) TYPE type_result
      RAISING
        cx_parameter_invalid.
    METHODS total
      RETURNING
        VALUE(result) TYPE type_result
      RAISING
        cx_parameter_invalid.
  PROTECTED SECTION.
  PRIVATE SECTION.

ENDCLASS.


CLASS zcl_grains IMPLEMENTATION.
  METHOD square.
    IF INPUT LE 0 OR INPUT GE 65.
      RAISE EXCEPTION TYPE cx_parameter_invalid.
    ENDIF.
    
    RESULT =  2 ** ( input - 1 ).
  ENDMETHOD.

  METHOD total.
      DATA(LV_COUNT) = 64.
        
    WHILE LV_COUNT NE 0.
      DATA(lv_temp) = me->square( lv_count ).
      ADD lv_temp to result.
      SUBTRACT 1 FROM lv_count.
    ENDWHILE.
  ENDMETHOD.


ENDCLASS.
