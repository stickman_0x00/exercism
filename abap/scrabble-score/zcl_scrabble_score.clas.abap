CLASS zcl_scrabble_score DEFINITION PUBLIC .

  PUBLIC SECTION.
    METHODS score
      IMPORTING
        input         TYPE string OPTIONAL
      RETURNING
        VALUE(result) TYPE i.
  PROTECTED SECTION.
  PRIVATE SECTION.

ENDCLASS.


CLASS zcl_scrabble_score IMPLEMENTATION.
  METHOD score.
    DATA(lv_temp) = input.
    TRANSLATE lv_temp TO UPPER CASE.

    DATA(LV_COUNTER) = 0.

    DO strlen( lv_temp ) TIMES.
      DATA(LV_CHAR) = lv_temp+lv_counter(1). " GET CHAR

      CASE lv_char.
        WHEN 'A' OR 'E' OR 'I' OR 'O' OR 'U' OR 'L' OR 'N' OR 'R' OR 'S' OR 'T'.
          ADD 1 TO result.
        WHEN 'D' OR 'G'.
          ADD 2 TO result.
        WHEN 'B' OR 'C' OR 'M' OR 'P'.
          ADD 3 TO result.
        WHEN 'F' OR 'H' OR 'V' OR 'W' OR 'Y'.
          ADD 4 TO result.
        WHEN 'K'.
          ADD 5 TO result.
        WHEN 'J' OR 'X'.
          ADD 8 TO result.
        WHEN 'Q' OR 'Z'.
          ADD 10 TO result.
        WHEN OTHERS.
      ENDCASE.

      ADD 1 TO lv_counter. " NEXT POSITION OF CHAR
    ENDDO.

  ENDMETHOD.

ENDCLASS.
