CLASS zcl_leap DEFINITION PUBLIC.
  PUBLIC SECTION.
    METHODS leap
      IMPORTING
        year          TYPE i
      RETURNING
        VALUE(result) TYPE abap_bool.
ENDCLASS.

CLASS zcl_leap IMPLEMENTATION.

  METHOD leap.
    result = ABAP_FALSE.

    if year MOD 4 NE 0.
      RETURN.
    ENDIF.
    
    if year MOD 100 EQ 0.
      if year MOD 400 NE 0.
        RETURN.
      endif.
    endif.
    
    result = ABAP_TRUE.
  ENDMETHOD.

ENDCLASS.
