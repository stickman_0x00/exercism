CLASS zcl_itab_aggregation DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    TYPES group TYPE c LENGTH 1.
    TYPES: BEGIN OF initial_numbers_type,
             group  TYPE group,
             number TYPE i,
           END OF initial_numbers_type,
           initial_numbers TYPE STANDARD TABLE OF initial_numbers_type WITH EMPTY KEY.

    TYPES: BEGIN OF aggregated_data_type,
             group   TYPE group,
             count   TYPE i,
             sum     TYPE i,
             min     TYPE i,
             max     TYPE i,
             average TYPE f,
           END OF aggregated_data_type,
           aggregated_data TYPE STANDARD TABLE OF aggregated_data_type WITH EMPTY KEY.

    METHODS perform_aggregation
      IMPORTING
        initial_numbers        TYPE initial_numbers
      RETURNING
        VALUE(aggregated_data) TYPE aggregated_data.
  PROTECTED SECTION.
  PRIVATE SECTION.

ENDCLASS.



CLASS zcl_itab_aggregation IMPLEMENTATION.
  METHOD perform_aggregation.
    " CREATE TABLE WITH DISTINCT GROUPS
    DATA(LT_groups) = initial_numbers.
    SORT lt_groups BY group.
    DELETE ADJACENT DUPLICATES FROM lt_groups COMPARING group.
    
    " LOOP GROUPS
    DATA: LS_NEW_ROW TYPE aggregated_data_type.
    LOOP AT lt_groups INTO DATA(Ls_group).
      CLEAR LS_NEW_ROW.
      
      " GROUP
      ls_new_row-group = ls_group-group.
      
      " GET ENTRIES OF THE GROUP      
      LOOP AT initial_numbers INTO DATA(LS_ENTRY) WHERE group = ls_group-group.
        ADD 1 TO ls_new_row-count. " COUNT
        IF ls_new_row-count EQ 1. "INITIALIZE
          ls_new_row-max = ls_entry-number.
          ls_new_row-min = ls_entry-number.
        ENDIF.
        
        ADD ls_entry-number TO ls_new_row-sum. " SUM
        
        IF ls_entry-number > ls_new_row-max. " MAX
          ls_new_row-max = ls_entry-number.
        ENDIF.
        IF ls_entry-number < ls_new_row-min. "MIN
          ls_new_row-min = ls_entry-number.
        ENDIF.
        
      ENDLOOP.
      
      " AVG
      ls_new_row-average = ls_new_row-sum / ls_new_row-count.
      
      APPEND ls_new_row TO aggregated_data.      
    ENDLOOP.
  ENDMETHOD.

ENDCLASS.
