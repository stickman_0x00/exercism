CLASS zcl_kindergarten_garden DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS plants
      IMPORTING
        diagram        TYPE string
        student        TYPE string
      RETURNING
        VALUE(results) TYPE string_table.

  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA students TYPE string_table.
    METHODS get_plant
      IMPORTING
        letter        TYPE c
      RETURNING
        VALUE(result) TYPE string.

ENDCLASS.


CLASS zcl_kindergarten_garden IMPLEMENTATION.


  METHOD plants.
    SPLIT diagram AT '\n' INTO DATA(lv_row_one) DATA(lv_row_two).
    FIND student(1) IN sy-abcde MATCH OFFSET DATA(lv_student_id).

    " First row
    DATA(lv_count) = lv_student_id * 2.
    APPEND get_plant( lv_row_one+lv_count(1) ) TO results.
    ADD 1 TO LV_COUNT.
    APPEND get_plant( lv_row_one+lv_count(1) ) TO results.

    " Second row
    SUBTRACT 1 FROM LV_COUNT.
    APPEND get_plant( lv_row_two+lv_count(1) ) TO results.
    ADD 1 TO LV_COUNT.
    APPEND get_plant( lv_row_two+lv_count(1) ) TO results.
  ENDMETHOD.

  METHOD get_plant.
    CASE letter.
      WHEN 'V'.
        result = 'violets'.
      WHEN 'R'.
        result = 'radishes'.
      WHEN 'C'.
        result = 'clover'.
      WHEN 'G'.
        result = 'grass'.
    ENDCASE.
  ENDMETHOD.


ENDCLASS.
