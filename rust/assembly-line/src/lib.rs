// This stub file contains items that aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

const H_PRODUCTION:f64 = 221_f64;

fn success_rate(n_production: u8) -> f64 {
	match n_production {
		0 => 0.0,
		1..=4=> 1.0,
		5..=8=> 0.9,
		9 | 10 => 0.77,
		_ => unimplemented!("sucess rate not supported: {}", n_production)
	}
}

pub fn production_rate_per_hour(speed: u8) -> f64 {
	(speed as f64) * H_PRODUCTION * success_rate(speed)
}

pub fn working_items_per_minute(speed: u8) -> u32 {
	(production_rate_per_hour(speed) as u32) / 60
}
