pub fn reverse(input: &str) -> String {
    if input == "" {
		return String::from("");
	}

	return input.rsplit("").collect();
	// let mut result: String = String::new();
    // for c in input.chars().rev() {
    //     result.push(c);
    // }
    // return result;
}
