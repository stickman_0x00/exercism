// This stub file contains items that aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

use std::collections::HashMap;

pub fn can_construct_note(magazine: &[&str], note: &[&str]) -> bool {
    let mut temp = HashMap::new();

    for word in magazine {
        *temp.entry(word).or_insert(0) += 1;
    }

    for word in note {
        match temp.get(word) {
            None | Some(0) => return false,
            _ => *temp.entry(word).or_insert(0) -= 1,
        };
    }
    true
    // for word in note {
    //     *temp.entry(word).or_insert(0) -= 1;
    // }

    // temp.values().all(|&x| x >= 0)
}
