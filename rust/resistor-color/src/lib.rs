use enum_iterator::{all, Sequence};
use int_enum::IntEnum;

#[derive(Debug, PartialEq, Eq, Sequence, IntEnum, Clone, Copy, PartialOrd, Ord)]
#[repr(u32)]
pub enum ResistorColor {
    Black = 0,
    Blue = 6,
    Brown = 1,
    Green = 5,
    Grey = 8,
    Orange = 3,
    Red = 2,
    Violet = 7,
    White = 9,
    Yellow = 4,
}

pub fn color_to_value(_color: ResistorColor) -> u32 {
    _color.int_value()
}

pub fn value_to_color_string(value: u32) -> String {
    match ResistorColor::from_int(value) {
        Ok(resistor) => format!("{:?}", resistor),
        Err(_) => format!("value out of range"),
    }
}

pub fn colors() -> Vec<ResistorColor> {
    let mut all_colors = all::<ResistorColor>().collect::<Vec<_>>();
    all_colors.sort();
    all_colors
}
