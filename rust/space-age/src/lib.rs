// The code below is a stub. Just enough to satisfy the compiler.
// In order to pass the tests you can add-to or change any of this code.

#[derive(Debug)]
pub struct Duration {
	seconds: u64
}

impl From<u64> for Duration {
    fn from(s: u64) -> Self {
		print!("IKA");
		Duration{seconds:s}
    }
}

pub trait Planet {
	const RELATIVE_TO_EARTH: f64 = 1.0;
    fn years_during(d: &Duration) -> f64 {
		d.seconds as f64 / (31557600_f64  * Self::RELATIVE_TO_EARTH)
    }
}

macro_rules! create_planet {
    ($planet:ident, $relative_year:expr) => {
        pub struct $planet;
        impl Planet for $planet {
            const RELATIVE_TO_EARTH: f64 = $relative_year;
        }
    };
}

create_planet!(Mercury, 0.2408467);
create_planet!(Venus, 0.61519726);
create_planet!(Earth, 1.0);
create_planet!(Mars,1.8808158 );
create_planet!(Jupiter, 11.862615);
create_planet!(Saturn, 29.447498);
create_planet!(Uranus, 84.016846);
create_planet!(Neptune, 164.79132);