// This stub file contains items that aren't used yet; feel free to remove this module attribute
// to enable stricter warnings.
#![allow(unused)]

pub struct Player {
    pub health: u32,
    pub mana: Option<u32>,
    pub level: u32,
}

impl Player {
    pub fn revive(&self) -> Option<Player> {
        match self.health {
            0 => Some(Player {
                health: 100,
                mana: match self.level {
                    0..=9 => None,
                    _ => Some(100),
                },
                level: self.level,
            }),
            _ => None,
        }
    }

    pub fn cast_spell(&mut self, mana_cost: u32) -> u32 {
        match self.mana {
            None => {
                self.health = match self.health >= mana_cost {
                    true => self.health - mana_cost,
                    false => 0,
                };
                0
            }
            Some(mana) => match mana_cost > mana {
                true => 0,
                false => {
                    self.mana = Some(mana - mana_cost);
                    mana_cost * 2
                }
            },
        }
    }
}
