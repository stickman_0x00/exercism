use std::collections::HashSet;

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
	let mut r :HashSet<&str> = HashSet::new();

    let word_uppercase = word.to_uppercase();


	for anagram in possible_anagrams {

		if !is_anagram(&word_uppercase, &anagram.to_uppercase()) {
			continue;
		}

		r.insert(*anagram);
	}

	r
}

fn is_anagram(word: &str, anagram: &str) -> bool {
	if word.len() != anagram.len() || word == anagram {
		return false;
	}

	let mut word_s = word.chars().collect::<Vec<char>>();
	word_s.sort_unstable();
	let mut anagram_s = anagram.chars().collect::<Vec<char>>();
	anagram_s.sort_unstable();

	if  word_s != anagram_s {
		return false;
	}

	true
}
