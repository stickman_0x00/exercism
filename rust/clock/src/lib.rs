use std::fmt;

#[derive(Debug, PartialEq)]
pub struct Clock {
	hours: i32,
	minutes: i32,
}

impl fmt::Display for Clock {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:02}:{:02}", self.hours, self.minutes)
    }
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        let mut c= Clock{hours,minutes};


		c.hours = hours % 24;
		c.hours += if minutes < 0 { (minutes - 60 + 1) / 60 } else { minutes / 60 };
		c.hours %= 24;
		if c.hours < 0 {
			c.hours += 24
		}

		c.minutes = ((minutes % 60) + 60) %60;
		c
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
		Self::new(self.hours, self.minutes + minutes)
    }

}
