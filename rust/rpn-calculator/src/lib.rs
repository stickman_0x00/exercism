#[derive(Debug)]
pub enum CalculatorInput {
    Add,
    Subtract,
    Multiply,
    Divide,
    Value(i32),
}

pub fn evaluate(inputs: &[CalculatorInput]) -> Option<i32> {
    println!("{:?}", inputs);
    if inputs.len() == 0 {
        return None;
    }

    let mut v = vec![];
    for input in inputs {
        let r = match input {
            CalculatorInput::Value(n) => *n,
            _ => {
                if v.len() < 2 {
                    return None;
                }

                let b = v.pop().unwrap();
                let a = v.pop().unwrap();

                match input {
                    CalculatorInput::Add => a + b,
                    CalculatorInput::Multiply => a * b,
                    CalculatorInput::Subtract => a - b,
                    CalculatorInput::Divide => a / b,
                    _ => return None,
                }
            }
        };
        v.push(r);
    }

    if v.len() != 1 {
        return None;
    }

    v.pop()
}
